<%-- 
    Document   : newjsp
    Created on : 2021/12/8, 下午 04:27:16
    Author     : 70609-1
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/test","root","");
        out.println("<br>");
        out.println(conn);
        out.println("<br/>");
        Statement s = conn.createStatement();
        ResultSet rs = s.executeQuery("select * from Login");
        while(rs.next())
        {
            out.println(rs.getString("id"));
            out.println(rs.getString("password"));
            out.println("<br/>");
        }

        conn.close();
        %>
    </body>
</html>
